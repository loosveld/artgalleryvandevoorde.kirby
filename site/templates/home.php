<?php snippet('header', array('robots' => 'index, follow')); ?>

<main id="home" data-barba="container" data-barba-namespace="home">
    <div class="container">
        <?php snippet('toggler'); ?>
        <div class="lg:grid grid-cols-12 gap-16">
            <?php snippet('aside'); ?>
            <div class="sections lg:col-span-9">
                <section id="hero" class="pt-16 lg:pt-40">
                    <a href="<?php echo $site->url((string) $site->language()) ?>"
                        class="block lg:hidden mb-12 md:w-6/12">
                        <img src="/assets/images/Vandevoorde-Artgallery-zwart-wit.svg" class="logo"
                            alt="<?php echo $site->title(); ?>">
                    </a>
                    <h1 class="uppercase"><?php echo $site->Title(); ?></h1>
                    <h2 class="font-text mt-6"><?php echo $site->subTitle(); ?></h2>
                    <div class="lg:hidden"><?php snippet('afspraak', ['hideModalContent' => true]) ?></div>
                </section>
                <hr class="my-8 md:mt-20 md:mb-16" />
                <?php snippet('kunstenaars', ['page' => $site->find('kunstenaars')]); ?>
                <?php snippet('evenementen', ['page' => $site->find('evenementen')]); ?>
            </div>
        </div>
    </div>
    <?php snippet('footer'); ?>
</main>

<?php snippet('scripts'); ?>