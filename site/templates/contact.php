<?php snippet('header', array('robots' => 'index, follow')); ?>

<main class="h-screen flex items-center" id="contact" data-barba="container" data-barba-namespace="contact">
    <?php snippet('toggler'); ?>
    <div class="container flex flex-col justify-center">
        <h1 class="mb-8">Stel een vraag</h1>
        <div class="flex w-full">
            <div class="w-1/2">
                <div class="mr-8 leading-loose">
                    <div class="mb-8">Via onderstaande kanalen of het formulier aan de rechterkant.</div>
                    <i class="fas fa-envelope w-4 mr-2 text-center" aria-hidden="true"></i><a href="mailto:<?php echo $site->email() ?>"><?php echo $site->email() ?></a><br />
                    <i class="fas fa-phone w-4 mr-2 text-center" aria-hidden="true"></i><?php echo $site->tel() ?><br />
                    <a href="https://www.facebook.com/meubelenVDV/" class="block " aria-hidden="true" target="_blank"><i class="fab fa-facebook-square  w-4 mr-2 text-center "></i>Onze Facebook-pagina </a>
                </div>
            </div>
            <div class="w-1/2">
                <form class="block" action="/contact" method="POST">
                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="name">
                            Voornaam + Naam
                        </label>
                        <input required id="name" type="text" name="name" value="<?php echo $form->old('name'); ?>" class="shadow appearance-none border rounded w-full py-2 px-3 leading-tight text-black focus:outline-none focus:shadow-outline" >
                    </div>
                    <div class="mb-6">
                        <label class="block text-white text-sm font-bold mb-2" for="email">
                            Email
                        </label>
                        <input required id="email" type="email" name="email" value="<?php echo $form->old('email'); ?>" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 leading-tight text-black focus:outline-none focus:shadow-outline" >
                    </div>
                    <div class="mb-6">
                        <label class="block text-white text-sm font-bold mb-2" for="message">
                            Uw Vraag
                        </label>
                        <textarea required id="message" name="message" rows="5" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 leading-tight text-black focus:outline-none focus:shadow-outline" ><?php echo $form->old('message'); ?></textarea>
                    </div>
                    <?php echo csrf_field(); ?>
                    <?php echo honeypot_field(); ?>
                    <div class="flex items-center justify-between">
                        <button class="bg-red text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                            Verstuur
                        </button>
                    </div>
                </form>
                <?php if ($form->success()) : ?>
                    Uw vraag is goed verstuurd!
                <?php else : ?>
                    <?php snippet('uniform/errors', ['form' => $form]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>

<?php snippet('scripts'); ?>