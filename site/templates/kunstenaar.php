<?php snippet('header', array('robots' => 'index, follow')) ?>

<main class="pt-12 container" id="kunstenaar" data-barba="container" data-barba-namespace="kunstenaar">

    <?php snippet('toggler'); ?>

    <div class="xl:fixed z-10 mb-16 " id="info">
        <a href=" <?php echo $site->url((string) $site->language()) ?>" class="block mb-8">
            <img src="/assets/images/Vandevoorde%20Art%20Gallery.png" class="logo" alt="<?php echo $site->title(); ?>">
        </a>
        <div class="flex items-center">
            <?php if ($prev = $page->prev()) : ?>
            <a href="<?= $prev->url() ?>" title="Vorige kunstenaar"
                class="fas fa-chevron-left no-underline block mr-8 mb-4 text-3xl lg:mb-0 lg:mr-4 lg:text-base"></a>
            <?php endif ?>
            <?php if ($next = $page->next()) : ?>
            <a href="<?= $next->url() ?>" title="Volgende kunstenaar"
                class="fas fa-chevron-right no-underline block mr-8 mb-4 text-3xl lg:mb-0 lg:mr-4 lg:text-base"></a>
            <?php endif ?>
            <a href="/#kunstenaars" class="underline-from-left leading-tight text-xl lg:text-base">Terug naar
                overzicht</a>
        </div>
        <h1 class="mt-2 mb-8 leading-tight"><?= $page->title()->html() ?></h1>
        <div id="info">
            <?php if ($page->techniek()) : ?>
            <b class="uppercase font-title">
                <?php foreach ($page->techniek()->split() as $techniek) :
						if ($techniek == "olieopdoek") : ?>
                olie op doek
                <?php elseif ($techniek == "acrylopdoek") : ?>
                acryl op doek
                <?php elseif ($techniek == "olieoppaneel") : ?>
                olie op paneel
                <?php elseif ($techniek == "bronzenbeeld") : ?>
                bronzen beeld
                <?php elseif ($techniek == "keramiek") : ?>
                keramiek
                <?php elseif ($techniek == "cement") : ?>
                cement schilderij
                <?php endif;
					endforeach ?>
            </b>
            <?php endif ?>
        </div>
        <?php if (!$page->text()->empty()) : ?>
        <div id="info"><?= $page->text()->kirbytext() ?></div>
        <?php endif; ?>
    </div>

    <div class="w-100 flex flex-col items-end" style="z-index:-1" id="gallery">
        <?php foreach ($page->images() as $kunstwerk) : ?>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="xl:w-1/2 mb-32">
            <a href="<?php echo $kunstwerk->resize(2000)->url(); ?>"
                title="<?= $kunstwerk->naam() ?> <?= $kunstwerk->afmetingen() ?> <?= $kunstwerk->techniek() ?>"><img
                    src="<?php echo $kunstwerk->resize(1000)->url(); ?>"
                    title="<?= $kunstwerk->naam() ?> <?= $kunstwerk->afmetingen() ?> <?= $kunstwerk->techniek() ?>"></a>
        </figure>
        <?php endforeach ?>
    </div>

</main>



<?php snippet('scripts'); ?>