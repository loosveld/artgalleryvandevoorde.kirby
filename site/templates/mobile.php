<?php snippet('header', array('robots' => 'index, follow')); ?>

<main class="bg-white h-screen flex items-center" id="mobile" data-barba="container" data-barba-namespace="mobile">
    <?php snippet('toggler'); ?>

    <div class="container flex items-center h-screen">
        <div>
            <ul class="font-extrabold font-title">
                <?php $index = 0; ?>
                <?php foreach ($site->children()->visible() as $page) : ?>
                    <li class="nav-item mb-2">
                        <a href="/#<?= $page->id() ?>" class="text-2xl lg:text-6l text-red lg:text-5xl no-underline"><?= $page->title()->html() ?></a>
                    </li>
                    <?php $index++; ?>
                <?php endforeach; ?>
                <li class="nav-item ">
                    <a href="mailto:<?php echo $site->email(); ?>" class="text-2xl lg:text-6l text-red lg:text-5xl no-underline">
                        Maak een afspraak
                        <svg height="25%" width="25%" viewBox="0 0 50 50" class="ml-4">
                            <polyline fill="none" stroke="white" points="20,15 30,25 20,35" />
                            <polyline fill="none" stroke="white" points="0,25 30,25" />
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</main>

<?php snippet('scripts'); ?>