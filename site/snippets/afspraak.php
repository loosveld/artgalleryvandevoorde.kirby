<?php $page = $pages->find('contact'); ?> 

<a data-modal-trigger="modal-afspraak" class="cursor-pointer text-white flex items-center justify-between sweep-to-top with-arrow-right mb-8 btn font-title p-4 bg-red no-underline uppercase font-bold mt-4 ">
    <?= $page->title() ?>
    <svg height="50" width="50" viewBox="0 0 50 50" class="ml-4 inline">
        <polyline fill="none" stroke="white" stroke-width="2px" points="20,15 30,25 20,35" />
        <polyline fill="none" stroke="white" stroke-width="2px" points="0,25 30,25" />
    </svg>
</a>
<br />

<?php if (!$hideModalContent) : ?>
    <template class="" data-modal-content="modal-afspraak">
        <div class="modal__container w-full h-full md:max-w-2xl md:h-auto text-white">
            <div class="leading-loose bg-red p-8 w-full h-full md:max-w-2xl md:h-auto">
                <h3><?= $page->title() ?></h3>
                <div class="mb-8"><?= $page->text()->kirbytext() ?></div>
                <i class="fas fa-envelope w-4 mr-2 text-center" aria-hidden="true"></i><a href="mailto:<?php echo $site->email() ?>"><?php echo $site->email() ?></a><br />
                <i class="fas fa-phone w-4 mr-2 text-center" aria-hidden="true"></i><?php echo $site->tel() ?><br />
                <i class="fab fa-whatsapp  w-4 mr-2 text-center" aria-hidden="true"></i><?php echo $site->tel() ?><br />
                <a href="https://www.facebook.com/meubelenVDV/" class="block " aria-hidden="true" target="_blank"><i class="fab fa-facebook-square  w-4 mr-2 text-center "></i>Facebook Message</a>
            </div>
        </div>
    </template>
<?php endif; ?>