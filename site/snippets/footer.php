  <footer class="bg-red pt-12 pb-8 text-sm leading-loose ">
    <div class="container">
      <div class="lg:grid grid-cols-12">
        <div class="lg:col-span-4 mb-12" id="contact">
          <h3 class="uppercase mb-8">Contactgegevens</h3>
          <div>
            <i class="fas fa-map-marker w-4 mr-2 text-center" aria-hidden="true"></i><a href="https://goo.gl/maps/mfEhqMCQFFq" target="_blank"><?php echo $site->address() ?></a><br />
            <i class="fas fa-envelope w-4 mr-2 text-center" aria-hidden="true"></i><a href="mailto:<?php echo $site->email() ?>"><?php echo $site->email() ?></a><br />
            <i class="fas fa-phone w-4 mr-2 text-center" aria-hidden="true"></i><?php echo $site->tel() ?><br />
            <i class="fas fa-info w-4 mr-2 text-center" aria-hidden="true"></i>Vlot bereikbaar met openbaar vervoer<br />
            <i class="fas fa-info w-4 mr-2 text-center" aria-hidden="true"></i>300m vanaf het station
            
            
            <div class="flex  mb-8">
              <a href="https://www.facebook.com/meubelenVDV/" class="fab fa-facebook-square w-4 mr-2 text-center text-4xl no-underline block mt-8" aria-hidden="true" target="_blank"> </a>
              <a href="https://www.instagram.com/vandevoorde.art/" class="fab fa-instagram w-4 mr-2 text-center text-4xl no-underline block mt-8 ml-8" aria-hidden="true" target="_blank"> </a>
            </div>

            <a href="https://kunstenmeubelhuisvandevoorde.us10.list-manage.com/subscribe/post?u=aa196448bb851cf5f501af055&id=59cdb442c0"
                        target="_blank"
                        class="cursor-pointer text-red flex items-center justify-between with-arrow-right mb-8 btn font-title p-4 bg-white no-underline uppercase font-bold mt-4 xl:w-3/4 mr-8">

                        Schrijf u in<br />op onze nieuwsbrief
                        <svg height="50" width="50" viewBox="0 0 50 50" class="ml-4 inline">
                            <polyline fill="none" stroke="#d00025" stroke-width="2px" points="20,15 30,25 20,35" />
                            <polyline fill="none" stroke="#d00025" stroke-width="2px" points="0,25 30,25" />
                        </svg>
                    </a>
          </div>
        </div>

        <div class="lg:col-span-5 mb-12" id="openingsuren">
          <h3 class="uppercase mb-8">OPENINGSUREN</h3>

          <?php echo $site->hours()->kirbyText(); ?>
        </div>

        <div class="lg:col-span-3 mb-8">
          <h3 class="uppercase  mb-8">ONTDEK OOK</h3>
          <a href="https://www.meubelhuisvandevoorde.be/" target="_blank" class="bg-white flex items-center justify-center p-4">
            <img src="/assets/images/Vandevoorde Meubelhuis.png" class="">
          </a>
        </div>
      </div>
  </footer>
