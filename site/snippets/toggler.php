<a data-modal-trigger="modal-mobile-menu" id="mobile-toggler" class=" mobile-toggler fixed <?php e($page->id() === "mobile", "active"); ?>">
    <div class="x"></div>
    <div class="y"></div>
</a>

<template class="" data-modal-content="modal-mobile-menu">
    <div class="container flex items-center h-screen">
        <div>
            <ul class="font-extrabold font-title">
                <?php $index = 0; ?>
                <?php foreach ($site->children()->visible() as $page) : ?>
                    <li class="nav-item mb-2">
                        <a href="/#<?= $page->id() ?>" class="text-3xl text-red lg:text-5xl no-underline"><?= $page->title()->html() ?></a>
                    </li>
                    <?php $index++; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</template>