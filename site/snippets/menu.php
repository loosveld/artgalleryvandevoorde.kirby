<nav id="navigation" role="navigation" class="nav hidden md:block">
	<ul>
		<?php foreach ($site->children()->visible() as $page) : ?>
			<?php if(($page->id() != 'evenementen' || ($page->id() == 'evenementen' && $page->hasVisibleChildren())) && $page->id() != 'contact'): ?>
			<li class="nav-item mb-4">
				<a href="#<?= $page->id() ?>" class="nav-link underline-from-left <?= e($page->isOpen(), 'nav-link-active') ?> "><?= $page->title()->html() ?></a>
			</li>
			<?php endif; ?>
		<?php endforeach; ?>
		<li class="nav-item mb-4">
			<a href="#contact" class="nav-link underline-from-left ">Contactgegevens</a>
		</li>
		<li class="nav-item mb-4">
			<a href="#openingsuren" class="nav-link underline-from-left ">Openingsuren</a>
		</li>
	</ul>
</nav>