<section id="<?php echo $page->id(); ?>" class="md:grid grid-cols-12 mb-24">
    <div class="md:col-span-6 mb-16">
        <h3 class="table-caption">
            <?php echo $page->title(); ?>
        </h3>
    </div>
    <div class="md:col-span-6 mb-16">
        <?php echo $page->text(); ?>
    </div>
    <div class="md:grid grid-cols-12 gap-8 col-span-12 col-span-12">
        <?php foreach ($page->children() as $artist) : ?>
            <div class="md:col-span-4 mb-8 md:mb-0">
                <figure class="artist" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="<?php echo $artist->url(); ?>" class="sweep-to-top-before" itemprop="contentUrl" data-size="<?php echo $artist->width(); ?>x<?php echo $artist->height(); ?>">
                        <?php echo $artist->files()->first()->resize(600); ?>
                    </a>
                    <figcaption itemprop="caption description"><?php echo $artist->title(); ?></figcaption>
                </figure>
            </div>
        <?php endforeach ?>
    </div>
</section>