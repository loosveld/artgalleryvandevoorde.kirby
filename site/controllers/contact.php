<?php

use Uniform\Form;

return function ($site, $pages, $page) {
    $form = new Form([
        'name' => [
            'rules' => ['required'],
        ],
        'email' => [
            'rules' => ['required', 'email'],
            'message' => 'Email is required',
        ],
        'message' => [],
    ]);

    if (r::is('POST')) {
        $form->emailAction([
            'to' => 'bramloosveld@gmail.com',
            'from' => 'bram@crispclean.be', //info@kunstenmeubelhuisvandevoorde.be
            'subject' => 'New message from the contact form',
            'service' => 'postmark',
            'service-options' => ['key' => 'fe5ea3bb-28a3-4a37-b917-c00176768f68']
        ]);
    }

    return compact('form');
};
