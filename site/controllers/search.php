<?php

return function($site, $pages, $page) {

  $query   = get('q');
  $results = page('kunstenaars')->search($query, 'title|text|stijl|techniek|');

  return array(
    'query'   => $query,
    'results' => $results,
  );

};