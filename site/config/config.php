<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'K2-PRO-9c19e7e978acb306baa1eabd102fe66d');

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/

c::set('auto-orient-images', true); 

c::set('debug', true);

c::set('lang.support', true);
c::set('lang.available', array('nl', 'fr', 'en'));
c::set('lang.detect', false);
c::set('lang.locale', true);

c::set('languages', array(
    array(
      'code'    => 'nl',
      'name'    => 'Dutch',
      'default' => true,
      'locale'  => 'nl_BE',
      'url'     => '/nl',
    ),
    array(
      'code'    => 'fr',
      'name'    => 'French',
      'locale'  => 'fr_FR',
      'url'     => '/fr',
    ),
    array(
      'code'    => 'en',
      'name'    => 'English',
      'locale'  => 'en_US',
      'url'     => '/en',
    ),
));