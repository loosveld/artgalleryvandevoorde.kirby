const { readdirSync, rename } = require("fs");
const { resolve } = require("path");
const { listeners } = require("process");

// Get path to image directory
let dirPath = resolve(__dirname, "content/1-evenementen");

// Get an array of the files inside the folder
const strings = readdirSync(dirPath);

// Loop through each file that was retrieved
strings.forEach((str) => {
    if (str.includes(".txt")) {
        rename(
            dirPath + `/${str}`,
            dirPath + `/${str.substring(0, str.length - 4)}.nl.txt`,
            (err) => console.log(err)
        );
    } else {
        let subPath = resolve(__dirname, `${dirPath}/${str}`);
        try {
            const sub = readdirSync(subPath);
            sub.forEach((str) => {
                if (str.includes(".txt")) {
                    rename(
                        subPath + `/${str}`,
                        subPath + `/${str.substring(0, str.length - 4)}.nl.txt`,
                        (err) => console.log(err)
                    );
                }
            });
        } catch (e) {}
    }
});
