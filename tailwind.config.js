module.exports = {
    theme: {
        container: {
            center: true,
            padding: "1.5rem",
        },
        colors: {
            transparent: "transparent",
            black: "#000000",
            white: "#fff",
            red: "#d00025",
            orange: "orange",
            gray: "#eee",
        },
        fontFamily: {
            title: ["Montserrat", "sans-serif"],
            text: ["'Exo 2'", "sans-serif"],
        },
    },
    variants: {},
    corePlugins: {},
    plugins: [],
};
