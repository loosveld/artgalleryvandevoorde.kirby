import { gsap } from "gsap";
import CookieConsent from "basic-cookie-consent/src/index.js";
import Navigation from "./navigation";
import barba from "@barba/core";
import SimpleLightbox from "simple-lightbox";
import BramModal from "bram-modal";
//import Mapbox from "./mapbox";
import SmoothScroll from "smooth-scroll";

const main = {};

const init = () => {
    main.navigation = new Navigation();

    main.cookieConsent = new CookieConsent({
        content: {
            desc:
                "Deze website gebruikt cookies om uw surfervaring te optimaliseren.",
            moreInfo: "Meer info",
        },
    });

    main.smoothScroll = new SmoothScroll('a[href*="#"]', {
        speed: 600,
        easing: "easeOutQuint",
        offset: () => 35,
        emitEvents: true,
    });

    window.addEventListener("scroll", () =>
        window.scrollY
            ? document.body.classList.add("scrolling")
            : document.body.classList.remove("scrolling")
    );

    main.simpleLightbox = new SimpleLightbox({
        elements: "#kunstenaar #gallery a",
        defaults: {
            nextBtnClass: "fas fa-chevron-left",
        },
    });

    /* MENU */

    main.mobileMenu = new BramModal({
        id: "modal-mobile-menu",
        classModifier: "mobileMenu",
        onShow: (modal) => {
            const menuItems = modal.elements.querySelectorAll(".nav-item a");
            menuItems.forEach((item) =>
                item.addEventListener("click", () => modal.closeModal())
            );
        },
    });

    main.afspraak = new BramModal({
        id: "modal-afspraak",
        classModifier: "afspraak",
    });
};

document.addEventListener("DOMContentLoaded", function () {
    init();
    // barba.init({
    //     transitions: [
    //         {
    //             leave(data) {
    //                 const done = this.async();
    //                 gsap.fromTo(
    //                     "#transition",
    //                     {
    //                         transformOrigin: "bottom right",
    //                         height: "100%",
    //                         top: "100%",
    //                         skewY: -15,
    //                         opacity: 0.5,
    //                     },
    //                     {
    //                         height: "100%",
    //                         top: 0,
    //                         skewY: 0,
    //                         duration: 0.5,
    //                         opacity: 1,
    //                         onComplete: done,
    //                     }
    //                 );
    //             },
    //             enter(data) {
    //                 if (location.hash) {
    //                     const top = document.querySelector(location.hash)
    //                         .offsetTop;
    //                     window.scrollTo(0, top);
    //                 } else {
    //                     window.scrollTo(0, 0);
    //                 }
    //                 init();
    //                 gsap.fromTo(
    //                     "#transition",
    //                     {
    //                         top: "0",
    //                         skewY: 0,
    //                     },
    //                     {
    //                         skewY: 3,
    //                         top: "-100%",
    //                     }
    //                 );
    //             },
    //         },
    //     ],
    // });
});
