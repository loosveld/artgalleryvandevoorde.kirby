import { gsap } from "gsap";

const Navigation = class {
    constructor() {
        this.toggler = document.querySelector("#mobile-toggler");
        this.links = document.querySelectorAll(".nav-link");
        this.overlayLinks = document.querySelectorAll(".nav-overlay .nav-link");

        this.initLinks();
        //this.initOverlayToggler();

        window.addEventListener("scrollStart", this.initLinks.bind(this));

        // $("body").on("scroll mousewheel touchmove", function(e) {
        //     e.preventDefault();
        //     e.stopPropagation();
        //     return false;
        // });
    }

    initLinks() {
        const hash = location.hash;
        this.links.forEach(link => {
            if (hash && link.href.includes(hash)) {
                link.classList.add("nav-link-active");
            } else {
                link.classList.remove("nav-link-active");
            }
        });
    }

    // initOverlayToggler() {
    //     if (this.overlayLinks)
    //         this.overlayLinks.forEach(link =>
    //             link.addEventListener("click", this.toggleOverlay.bind(this))
    //         );

    //     if (this.toggler) {
    //         this.toggler.addEventListener(
    //             "click",
    //             this.toggleOverlay.bind(this)
    //         );
    //     }
    // }

    // toggleOverlay() {
    //     document.body.classList.toggle("nav-overlay-show");
    //     if (document.body.classList.contains("nav-overlay-show")) {
    //         this.overlayLinks.forEach((link, i) => {
    //             gsap.fromTo(
    //                 link,
    //                 {
    //                     opacity: 0
    //                     //webkitClipPath:
    //                     //"polygon(0 100%, 100% 100%, 100% 100%, 0% 100%)"
    //                 },
    //                 {
    //                     opacity: 1,
    //                     delay: (i + 1) * 0.2,
    //                     duration: 0.1
    //                     //webkitClipPath: "polygon(0 100%, 100% 100%, 100% 0, 0 0)"
    //                 }
    //             );

    //             link.addEventListener("click", this.toggleOverlay.bind(this));
    //         });
    //     }
    // }
};

export default Navigation;
