Title: Solo Tentoonstelling Irénée Duriez en Jan van 't Hoff

----

Coverimage: ireneeduriez.jpg

----

Info: 

----

Startdatum: 2018-11-04

----

Einddatum: 2018-12-15

----

Text: 

Solo tentoonstelling van Irénée Duriez opgeluisterd door de schilderijen van Jan van 't Hoff. Tentoonstelling van 4 november tot 15 december. Vernissage op 4 november om 14 uur. Iedereen welkom.

Irénée Duriez zal aanwezig zijn.