Title: Marloes Wijtsma

----

Stijl: 

----

Techniek: olieopdoek

----

Text: 

Marloes werkt in gemengde technieken op grote doeken en maakt kruisingen tussen schilderijen en tekeningen.Ze vindt in ieder schilderij een boeiende balans tussen kleurvlak en lijntekening, abstractie en bijna figuratieve illustratie, tussen lege ruimte en bonte drukte. Er valt veel te ontdekken aan kleine, verrassende details en ze beschikt over een fraai handschrift.

Op moment tekent en schildert Marloes veel boeketten en bloesems en maakt zij haar eigentijdse vertaling van de bloemstillevens uit de Gouden eeuw. De verf vloeit, drupt en spat op soms onbewerkt linnen met daarin verwerkt de gedetailleerde tekeningen van bloemen en bladeren.

----

Kunstwerken: marloes-wijtsma-bouquet-blues-140-x-110.jpg, marloes-wijtsma-bouquet-with-daisies-140-x-110-min.jpg, marloes-wijtsma-ranonkel-90-x90.jpg, marloes-wijtsma-valentine-bouquet-2.jpg