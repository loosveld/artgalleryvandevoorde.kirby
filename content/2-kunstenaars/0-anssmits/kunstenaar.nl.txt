Title: Ans Smits

----

Text: 

Ans' liefde voor zee en strand, mens en dier werken aanstekelijk. Een houding, emotie, blik of moment legt ze op vast in haar eigen stijl. Ans zoekt binding met wat ze schildert, het moet haar raken. Deze band weet ze perfect vast te leggen op doek.

>“Ik kijk en interpreteer. Ik haal het naar me toe, zoom in, filter, kies en geef weer.”

----

Stijl: figuratief

----

Techniek: olieopdoek

----

Kunstwerken: 15794.jpg, 17028.jpg, 17355.jpg, 17356.jpg, 20693.jpg

----

Pictures: 