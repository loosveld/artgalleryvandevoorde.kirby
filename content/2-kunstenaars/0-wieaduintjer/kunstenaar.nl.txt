Title: Wiea Duintjer

----

Stijl: 

----

Techniek: bronzenbeeld

----

Text: Wiea Duintjer is een vrouw met zin in het leven en dat vind je terug in haar werk. Al haar sculpturen stralen humor en levenslust uit. Bij het zien van haar werk word je oprecht vrolijk. Vrouwen, dieren en symbolieken zijn steeds terugkerende uitgangspunten van Wiea.

>“Wiea Duintjer toont in haar werk een groot levensgeluk, gegrepen in een klein moment.”
