Title: Paul De Vos

----

Stijl: 

----

Techniek: keramiek

----

Text: Pauls fascinatie voor de alchemie van aarde en vuur, gecombineerd met zijn plastische aanvoelen leidt tot op de dag van vandaag tot een fascinerende productie. In de geest van de kunstenaar ontstaan grillige vormen die refereren aan honden, mensen, en andere wezens, en onder zijn handen nemen die vormen de kleur, textuur en kromming aan van lieflijk-waanzinnige droomdieren.

----

Kunstwerken: 